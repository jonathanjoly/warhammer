'use strict';

/**
 * @ngdoc function
 * @name warhammerApp.controller:CreateCtrl
 * @description
 * # CreateCtrl
 * Controller of the warhammerApp
 */
angular.module('warhammerApp')
  .controller('CreateCtrl',['$scope','$location','dataSet','session', function ($scope,$location,dataSet,session) {

	  $scope.dataSet = dataSet;

		// INITIALIZE
		$scope.carriereTalentsChoix=[];
		$scope.carriereCompetencesChoix = [];
		$scope.selectedSexe ="M";
		$scope.promotion = {selected:"",choices:[]};
		$scope.step = 1;
		$scope.character = {};


		var stats = ['cc','ct','f','e','ag','int','fm','soc','a','b','m','mag'];


		// ON RACE CHANGE ---------------------------------------------------------------------------
		// ON RACE CHANGE ---------------------------------------------------------------------------
		// ON RACE CHANGE ---------------------------------------------------------------------------
		// ON RACE CHANGE ---------------------------------------------------------------------------
		// ON RACE CHANGE ---------------------------------------------------------------------------

		$scope.onChangeRace=function(){
			var generateCharacteristics = new GenerateCharacteristics();
			$scope.characteristics =  generateCharacteristics.generate($scope.selectedRaces);
			$scope.characteristics.set('A',1);

			$scope.misericordeStat = generateCharacteristics.generateAverage($scope.selectedRaces);
			$scope.misericordeDispo = true;

			var generateCarrer = new GenerateCareer;
			$scope.carrieres = generateCarrer.generate($scope.selectedRaces);


			var generateBonusRaciaux = new GenerateRacialBonus;
			$scope.racialCompetencesChoices = generateBonusRaciaux.getSkillChoices($scope.selectedRaces);
			$scope.racialTalentsChoices=	generateBonusRaciaux.getTalentChoices($scope.selectedRaces);


			$scope.onChangeSexe();
			$scope.onChangeCareer($scope.carrieres[0]);

		};



		// DEMANDER MISERICORDE ---------------------------------------------------------------------
		// DEMANDER MISERICORDE ---------------------------------------------------------------------
		// DEMANDER MISERICORDE ---------------------------------------------------------------------
		// DEMANDER MISERICORDE ---------------------------------------------------------------------
		// DEMANDER MISERICORDE ---------------------------------------------------------------------

		$scope.demanderMisericorde=function(carac){
			$scope.characteristics.set(carac,$scope.misericordeStat.get(carac));
			$scope.misericordeDispo = false;
		};


		// ON SEXE CHANGE ---------------------------------------------------------------------------
		// ON SEXE CHANGE ---------------------------------------------------------------------------
		// ON SEXE CHANGE ---------------------------------------------------------------------------
		// ON SEXE CHANGE ---------------------------------------------------------------------------
		// ON SEXE CHANGE ---------------------------------------------------------------------------

		$scope.onChangeSexe=function(){
			var generateFamilleOrigine = new GenerateOrigin();
			var generateNom = new GenerateName;
			var generatePhysicalTrait = new GeneratePhysicalTrait();

			$scope.details={};

			$scope.details.origine = generateFamilleOrigine.generate($scope.selectedRaces,$scope.selectedSexe);
			$scope.details.nom = generateNom.generate($scope.selectedRaces,$scope.selectedSexe);
			$scope.details.trait = generatePhysicalTrait.generate($scope.selectedRaces,$scope.selectedSexe);
			$scope.details.sexe = $scope.selectedSexe;
			$scope.details.race = $scope.selectedRaces;
		};

		// ON CHANGE CAREERS ------------------------------------------------------------------------
		// ON CHANGE CAREERS ------------------------------------------------------------------------
		// ON CHANGE CAREERS ------------------------------------------------------------------------
		// ON CHANGE CAREERS ------------------------------------------------------------------------
		// ON CHANGE CAREERS ------------------------------------------------------------------------

		$scope.onChangeCareer=function(carriere){

			$scope.selectedCareer = carriere;
			var carriere = $scope.dataSet.index[$scope.selectedCareer];

			$scope.carriereCompetencesChoix = [];
			$scope.carriereTalentsChoix = [];
			$scope.dotations = [];

			for(var i=0; i< carriere.competences.length;i++){

				$scope.carriereCompetencesChoix.push({selection:carriere.competences[i][0],choix:carriere.competences[i]});
			}



			for(var i=0; i< carriere.talents.length;i++){
				$scope.carriereTalentsChoix.push({selection:carriere.talents[i][0],choix:carriere.talents[i]});
			}

			for(var i=0; i< carriere.objets.length;i++){
				$scope.dotations.push({selection:carriere.objets[i][0].id,choix:carriere.objets[i]});
			}

		};

		// VALIDATE ---------------------------------------------------------------------------------
		// VALIDATE ---------------------------------------------------------------------------------
		// VALIDATE ---------------------------------------------------------------------------------
		// VALIDATE ---------------------------------------------------------------------------------
		// VALIDATE ---------------------------------------------------------------------------------

		$scope.valider=function(){

			var perso = new Character(); // Create our object

			perso.details =$scope.details; // Set the details information

			perso.nouvelleCarriere($scope.selectedCareer);
			perso.caractBase = $scope.characteristics;

			// TALENTS ------------------------------------------------
			// TALENTS ------------------------------------------------
			// TALENTS ------------------------------------------------

			// Racial
			for(var i=0; i< $scope.racialTalentsChoices.length;i++){
				perso.ajoutTalent($scope.racialTalentsChoices[i].selection);
			}

			// Carriere
			for(var i=0; i<$scope.carriereTalentsChoix.length;i++){
				perso.ajoutTalent($scope.carriereTalentsChoix[i].selection);

				var index = $scope.carriereTalentsChoix[i].choix.indexOf($scope.carriereTalentsChoix[i].selection);
				var array = $scope.carriereTalentsChoix[i].choix;
				array.splice(index, 1);


				var promotion = [];
				for(var j=0;j<array.length;j++){
					promotion.push({id:array[j],type:'TAL'});
				}


				$scope.promotion.choices=$scope.promotion.choices.concat(promotion);

			}


			// COMPETENCES --------------------------------------------
			// COMPETENCES --------------------------------------------
			// COMPETENCES --------------------------------------------

			// Racial
			for(var i=0; i<$scope.racialCompetencesChoices.length;i++){
				perso.ajoutCompetence($scope.racialCompetencesChoices[i].selection,$scope.selectedRaces);
			}

			// Carriere
			for(var i=0; i<$scope.carriereCompetencesChoix.length;i++){
				perso.ajoutCompetence($scope.carriereCompetencesChoix[i].selection,$scope.selectedCareer);

				var index = $scope.carriereCompetencesChoix[i].choix.indexOf($scope.carriereCompetencesChoix[i].selection);


				var array = $scope.carriereCompetencesChoix[i].choix;
				array.splice(index, 1);

				var promotion = [];
				for(var j=0;j<array.length;j++){
					promotion.push({id:array[j],type:'COMP'});
				}

				$scope.promotion.choices=$scope.promotion.choices.concat(promotion);
			}



			// CARACTERISTIQUE ----------------------------------------
			// CARACTERISTIQUE ----------------------------------------
			// CARACTERISTIQUE ----------------------------------------

			var carriere = $scope.dataSet.index[$scope.selectedCareer];
			for(var i=0;i<stats.length;i++){
				if(carriere[stats[i]] != ''){
					$scope.promotion.choices.push({id:stats[i],type:'CAR'});
				}
			}

			$scope.step = 2;
			$scope.promotion.selected = $scope.promotion.choices[0];
			$scope.character = perso;

            // DOTATIONS
            // DOTATIONS
            // DOTATIONS

      for(var i=0;i< $scope.dotations.length;i++){

          var object = {};
          for(var j=0; j <$scope.dotations[i].choix.length;j++){
            if($scope.dotations[i].choix[j].id === $scope.dotations[i].selection){
              object = $scope.dotations[i].choix[j];
            }
          }

        var original = $scope.dataSet.index[object.id];

        var objectToAdd = {
          id:object.id,
          qualite:object.qualite,
          quantite:object.quantite,
          load: original.encombrement,
          name: original.nom
        };


        switch (objectToAdd.id.substring(0, 4)) {
          case 'ARMU':
              objectToAdd.area = original.zone;
              objectToAdd.pa = original.pa;
              $scope.character.addArmor(objectToAdd);
            break;
          case 'ARME':

              objectToAdd.portee= original.portee;
              objectToAdd.degat= original.degat;
              objectToAdd.recharge = original.recharge;
              objectToAdd.attributs = original.attributs;
              $scope.character.addWeapon(objectToAdd);
              break;
          case 'OBAR':

            break;
          default:
            $scope.character.addObject(objectToAdd);

        }

        //$scope.character.addObject();
      }


		};




		// FINISH -----------------------------------------------------------------------------------
		// FINISH -----------------------------------------------------------------------------------
		// FINISH -----------------------------------------------------------------------------------
		// FINISH -----------------------------------------------------------------------------------
		// FINISH -----------------------------------------------------------------------------------
		$scope.end=function(){

			$scope.character.caractPrise= new Characteristics();



			switch($scope.promotion.selected.type){
				case 'TAL':
					$scope.character.ajoutTalent($scope.promotion.selected.id);
					break;
				case 'COMP':
					$scope.character.ajoutCompetence($scope.promotion.selected.id,$scope.selectedCareer);
					break;
				case 'CAR':


					var value = 5;

					if($scope.promotion.selected.id == 'a' || $scope.promotion.selected.id == 'b' || $scope.promotion.selected.id == 'm' ||$scope.promotion.selected.id=='mag'){
						value = 1;
					}

					$scope.character.caractPrise.set($scope.promotion.selected.id.toUpperCase(),value);

					break;
			}


			session.selectedCharacter=$scope.character;
			$location.path('/edit');

		};




  }]);
