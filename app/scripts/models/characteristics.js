'use strict';
function Characteristics(){

	this.cc=0;
	this.ct=0;
	this.f=0;
	this.e=0;
	this.ag=0;
	this.int=0;
	this.fm=0;
	this.soc=0;
	this.a=0;
	this.b=0;
	this.m=0;
	this.mag=0;
	this.pf=0;
	this.pd=0;
	
	this.get=function(CharacteristicId){
		var value=-1;
	
		switch(CharacteristicId){
			case "CC": value= this.cc ;break; 
			case "CT": value= this.ct ;break; 
			case "F": value= this.f ;break; 
			case "E": value= this.e ;break; 
			case "AG": value= this.ag ;break; 
			case "INT": value= this.int ;break; 
			case "FM": value= this.fm ;break; 
			case "SOC": value= this.soc ;break; 
			case "A": value= this.a ;break; 
			case "B": value= this.b ;break; 
			case "BF": value= Math.floor(this.f/10) ;break; 
			case "BE": value= Math.floor(this.e/10);break; 
			case "M": value= this.m ;break; 
			case "MAG": value= this.mag ;break; 
			case "PF": value= this.pf ;break; 
			case "PD": value= this.pd ;break;
		}
		
		return value;
	};
	
	this.set=function(CharacteristicId,value){

		switch(CharacteristicId){
			case "CC":  this.cc =value;break; 
			case "CT":  this.ct =value;break; 
			case "F":  this.f =value;break; 
			case "E":  this.e =value;break; 
			case "AG":  this.ag =value;break; 
			case "INT":  this.int =value;break; 
			case "FM":  this.fm =value;break; 
			case "SOC":  this.soc =value;break; 
			case "A":  this.a =value;break; 
			case "B":  this.b =value;break; 
			case "M":  this.m =value;break; 
			case "MAG":  this.mag =value;break; 
			case "PF":  this.pf =value;break; 
			case "PD":  this.pd =value;break;
		}

	};
	
	
}