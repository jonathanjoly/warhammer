'use strict';
/*  Livre de base Table 2-1 , 2-2 , 2-3
*/

function GenerateCharacteristics(){
	
	this.table=[];
		
	this.table["RACE-ELFE"]={};
	this.table["RACE-ELFE"]["CC"]= 20;
	this.table["RACE-ELFE"]["CT"]= 30;
	this.table["RACE-ELFE"]["F"]= 20;
	this.table["RACE-ELFE"]["E"]= 20;
	this.table["RACE-ELFE"]["AG"]= 30;
	this.table["RACE-ELFE"]["INT"]= 20;
	this.table["RACE-ELFE"]["FM"]= 20;
	this.table["RACE-ELFE"]["SOC"]= 20;
	this.table["RACE-ELFE"]["M"]= 5;
	this.table["RACE-ELFE"]["B"] = [9,9,9,10,10,10,11,11,11,12];
	this.table["RACE-ELFE"]["PD"] = [1,1,1,1,2,2,2,2,2,2]; 
	
	this.table["RACE-HALF"]={};
	this.table["RACE-HALF"]["CC"]= 10;
	this.table["RACE-HALF"]["CT"]= 30;
	this.table["RACE-HALF"]["F"]= 10;
	this.table["RACE-HALF"]["E"]= 10;
	this.table["RACE-HALF"]["AG"]= 30;
	this.table["RACE-HALF"]["INT"]= 20;
	this.table["RACE-HALF"]["FM"]= 20;
	this.table["RACE-HALF"]["SOC"]= 30;
	this.table["RACE-HALF"]["M"]= 4;
	this.table["RACE-HALF"]["B"] = [8,8,8,9,9,9,10,10,10,11];
	this.table["RACE-HALF"]["PD"] = [2,2,2,2,2,2,2,3,3,3]; 
	
	this.table["RACE-HUMA"]={};
	this.table["RACE-HUMA"]["CC"]= 20;
	this.table["RACE-HUMA"]["CT"]= 20;
	this.table["RACE-HUMA"]["F"]= 20;
	this.table["RACE-HUMA"]["E"]= 20;
	this.table["RACE-HUMA"]["AG"]= 20;
	this.table["RACE-HUMA"]["INT"]= 20;
	this.table["RACE-HUMA"]["FM"]= 20;
	this.table["RACE-HUMA"]["SOC"]= 20;
	this.table["RACE-HUMA"]["M"]= 4;
	this.table["RACE-HUMA"]["B"] = [10,10,10,11,11,11,12,12,12,13];
	this.table["RACE-HUMA"]["PD"] = [2,2,2,2,3,3,3,3,3,3]; 
	
	this.table["RACE-NAIN"]={};
	this.table["RACE-NAIN"]["CC"]= 30;
	this.table["RACE-NAIN"]["CT"]= 20;
	this.table["RACE-NAIN"]["F"]= 20;
	this.table["RACE-NAIN"]["E"]= 30;
	this.table["RACE-NAIN"]["AG"]= 10;
	this.table["RACE-NAIN"]["INT"]= 20;
	this.table["RACE-NAIN"]["FM"]= 20;
	this.table["RACE-NAIN"]["SOC"]= 10;
	this.table["RACE-NAIN"]["M"]= 3;
	this.table["RACE-NAIN"]["B"] = [11,11,11,12,12,12,13,13,13,14];
	this.table["RACE-NAIN"]["PD"] = [1,1,1,1,2,2,2,3,3,3]; 


	this.generate= function(raceId){
		var characteristics = new Characteristics();
		var dice = new Dice();
		
		characteristics.set("CC",this.compute(raceId,"CC",dice.D10()+dice.D10()));
		characteristics.set("CT",this.compute(raceId,"CT",dice.D10()+dice.D10()));
		characteristics.set("F",this.compute(raceId,"F",dice.D10()+dice.D10()));
		characteristics.set("E",this.compute(raceId,"E",dice.D10()+dice.D10()));
		characteristics.set("AG",this.compute(raceId,"E",dice.D10()+dice.D10()));
		characteristics.set("INT",this.compute(raceId,"INT",dice.D10()+dice.D10()));
		characteristics.set("FM",this.compute(raceId,"FM",dice.D10()+dice.D10()));
		characteristics.set("SOC",this.compute(raceId,"SOC",dice.D10()+dice.D10()));
		
		characteristics.set("M",this.table[raceId]["M"]);
		

		
		characteristics.set("B",this.define(raceId,"B",dice.D10()));
		characteristics.set("PD",this.define(raceId,"PD",dice.D10()));
		
		return characteristics;
		
	};

	this.compute= function(raceId,caracId,diceRes){
		return this.table[raceId][caracId] + diceRes;
	};
	
	this.define= function(raceId,caracId,diceRes){
		return this.table[raceId][caracId][diceRes-1];
	};

	
	this.generateAverage= function(raceId){
		var characteristics = new Characteristics();

		characteristics.set("CC",this.compute(raceId,"CC",11));
		characteristics.set("CT",this.compute(raceId,"CT",11));
		characteristics.set("F",this.compute(raceId,"F",11));
		characteristics.set("E",this.compute(raceId,"E",11));
		characteristics.set("AG",this.compute(raceId,"E",11));
		characteristics.set("INT",this.compute(raceId,"INT",11));
		characteristics.set("FM",this.compute(raceId,"FM",11));
		characteristics.set("SOC",this.compute(raceId,"SOC",11));
		
		return characteristics;
		
	};







}




	