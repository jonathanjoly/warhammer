# coding=utf-8
import xlrd, os, json
from sheets import *
from armes import *
from talents import *
from competences import *
from careers import *

path = "Dictionaire.xlsx"

sheetDict = getMappingSheets()
fieldDict = getMappingFields()



workbook = xlrd.open_workbook(path)



generateArmes(workbook,fieldDict)
generateTalents(workbook,fieldDict)
generateCompetences(workbook,fieldDict)
generateCareers(workbook,fieldDict)






#LOOP ON SHEETS 
for sheetName, fileName in sheetDict.items():
    sheet= workbook.sheet_by_name(sheetName)
    
    list = []
    
    #LOOP ON ROW 
    for rowIndex in range(1,sheet.nrows):
        
        object = {}
        
        #LOOP ON COLS 
        for colIndex in range(sheet.ncols):    
            columnName = sheet.cell(0,colIndex).value      
            object[fieldDict[columnName]]= sheet.cell(rowIndex,colIndex).value
        
        list.append(object)
    
    with open('../app/api/'+fileName+'.json', 'w') as f:
        json.dump(list, f)


#for sheetIndex in range(0, book.nsheets):
    #sheet = book.sheet_by_index(sheetIndex)
    
    #print sheet.name
    
    
    
