import json

def generateCompetences(workbook,fieldDict):
    
    competenceTalent = {}
    
    sheet= workbook.sheet_by_name('CompetenceTalent')
    
    for rowIndex in range(1,sheet.nrows):
        competence = sheet.cell(rowIndex,1).value 
        talent = sheet.cell(rowIndex,2).value
        
        if not competence in competenceTalent:
            competenceTalent[competence]=[]
        
        competenceTalent[competence].append(talent)
    
    
    sheet= workbook.sheet_by_name('Competences')
    list = []
     
    #LOOP ON ROW 
    for rowIndex in range(1,sheet.nrows):
    
        object = {}
    
        #LOOP ON COLS 
        for colIndex in range(sheet.ncols):    
            columnName = sheet.cell(0,colIndex).value      
            object[fieldDict[columnName]]= sheet.cell(rowIndex,colIndex).value
            
        id = object['id']
        
        if id in competenceTalent:
            object['talents'] = competenceTalent[id]
            
        list.append(object)
    
    with open('../app/api/skills.json', 'w') as f:
        json.dump(list, f)
        