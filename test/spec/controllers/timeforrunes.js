'use strict';

describe('Controller: TimeforrunesCtrl', function () {

  // load the controller's module
  beforeEach(module('warhammerApp'));

  var TimeforrunesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TimeforrunesCtrl = $controller('TimeforrunesCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(TimeforrunesCtrl.awesomeThings.length).toBe(3);
  });
});
