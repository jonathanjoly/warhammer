'use strict';
function Qualities()  {
	
	this.list =[	
	       	{id:"QUAL-EXCE",nom: "Exceptionelle"},
	    	{id:"QUAL-BONN",nom: "Bonne"},
	    	{id:"QUAL-MOYE",nom: "Moyenne"},
	    	{id:"QUAL-MEDI",nom: "Médiocre"}];
		
	  this.get = function() {
	          return this.list;
	  };
}