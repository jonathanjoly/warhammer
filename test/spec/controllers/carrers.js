'use strict';

describe('Controller: CarrersCtrl', function () {

  // load the controller's module
  beforeEach(module('warhammerApp'));

  var CarrersCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CarrersCtrl = $controller('CarrersCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(CarrersCtrl.awesomeThings.length).toBe(3);
  });
});
