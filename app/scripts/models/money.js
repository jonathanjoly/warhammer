'use strict';
function Money (){

	/*---------------------------------------------------------------------------------------------------------------------------*/
	/*---------------------------------------------------------------------------------------------------------------------------*/
	/*---------------------------------------------------------------------------------------------------------------------------*/
	/*---------------------------------------------------------------------------------------------------------------------------*/
	/*---------------------------------------------------------------------------------------------------------------------------*/

	
	this.stringToMoney =function(amount){
		var money=0;

		if(amount){
			var index = amount.indexOf('co');
			if(-1 !== index){
				money= amount.replace(" co","")*240;
			}
			else{
				var index = amount.indexOf('pa');
				if(-1 !== index){
					money= amount.replace(" pa","")*12;
				}
				else{
					money= amount.replace(" s","")*1;
				}
			}
		}

		return this.scToMoney(money);
	};
	/*---------------------------------------------------------------------------------------------------------------------------*/
	/*---------------------------------------------------------------------------------------------------------------------------*/
	/*---------------------------------------------------------------------------------------------------------------------------*/
	/*---------------------------------------------------------------------------------------------------------------------------*/
	/*---------------------------------------------------------------------------------------------------------------------------*/	
	this.scToMoney=function(amount){

		var pa = Math.floor(amount/12);
		var sc = amount%12;
		var co = Math.floor(pa/20);
		
		pa = pa%20; 

		return {co:co,pa:pa,s:sc};
	};
	/*---------------------------------------------------------------------------------------------------------------------------*/
	/*---------------------------------------------------------------------------------------------------------------------------*/
	/*---------------------------------------------------------------------------------------------------------------------------*/
	/*---------------------------------------------------------------------------------------------------------------------------*/
	/*---------------------------------------------------------------------------------------------------------------------------*/	
	this.multiply=function(money,multiple){
		

	
		var money = money.co * 240+ money.pa*12 +money.s;
		money = money * multiple;
		
		return this.scToMoney(money);
	};
	
	this.divide=function(argent,diviseur){
		var somme = argent.co * 240+ argent.pa*12 +argent.s;
		somme = Math.round(somme / diviseur);
		return this.scToMoney(somme);
	};
	this.add=function(argent1,argent2){
		var somme1 = argent1.co * 240+ argent1.pa*12 +argent1.s;
		var somme2 = argent2.co * 240+ argent2.pa*12 +argent2.s;
		return this.scToMoney(somme1+somme2);
	};
	this.adaptpriceQuality=function(argent,qualiteId){	

		switch(qualiteId){
			case "QUAL-EXCE": argent = this.multiply(argent,10); break;
			case "QUAL-BONN": argent = this.multiply(argent,5); break;
			case "QUAL-MEDI": argent = this.divide(argent,2);break;
		}
		return argent;
	};
		
	this.toString=function(argentStruct){
		var montantString="";
		
		if(argentStruct.co >0){
			montantString += argentStruct.co+" co";
			if(argentStruct.pa >0){
				montantString +=" "+ argentStruct.pa+" pa";
			}
			if(argentStruct.s >0){
				montantString +=" "+ argentStruct.s+" s";
			}
		}
		else{
			if(argentStruct.pa >0){
				montantString += argentStruct.pa+" pa";
				if(argentStruct.s >0){
					montantString +=" "+ argentStruct.s+" s";
				}
			}
			else{
				if(argentStruct.s >0){
					montantString += argentStruct.s+" s";
				}
			}
		}
		return montantString;
	};
	
}