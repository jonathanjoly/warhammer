'use strict';
//Libvre de base 5-2 5-3
//disponibility.js
function Disponibility(){
	
	this.table={};
	
	this.table["DISPO-TRRA"]={};
	
	this.table["DISPO-TRRA"]["100"]="DIFF-MAJE";
	this.table["DISPO-TRRA"]["1000"]="DIFF-TRDI";
	this.table["DISPO-TRRA"]["10000"]="DIFF-DIFF";
	this.table["DISPO-TRRA"]["10001"]="DIFF-ASDI";
	
	this.table["DISPO-RARE"]={};
	this.table["DISPO-RARE"]["100"]="DIFF-TRDI";
	this.table["DISPO-RARE"]["1000"]="DIFF-DIFF";
	this.table["DISPO-RARE"]["10000"]="DIFF-ASDI";
	this.table["DISPO-RARE"]["10001"]="DIFF-MOYE";
	
	
	this.table["DISPO-INHA"]={};
	this.table["DISPO-INHA"]["100"]="DIFF-DIFF";
	this.table["DISPO-INHA"]["1000"]="DIFF-ASDI";
	this.table["DISPO-INHA"]["10000"]="DIFF-MOYE";
	this.table["DISPO-INHA"]["10001"]="DIFF-ASFA";
	
	
	this.table["DISPO-ASCO"]={};
	this.table["DISPO-ASCO"]["100"]="DIFF-ASDI";
	this.table["DISPO-ASCO"]["1000"]="DIFF-MOYE";
	this.table["DISPO-ASCO"]["10000"]="DIFF-ASFA";
	this.table["DISPO-ASCO"]["10001"]="DIFF-FACI";
	
	
	this.table["DISPO-COUR"]={};
	this.table["DISPO-COUR"]["100"]="DIFF-MOYE";
	this.table["DISPO-COUR"]["1000"]="DIFF-ASFA";
	this.table["DISPO-COUR"]["10000"]="DIFF-FACI";
	this.table["DISPO-COUR"]["10001"]="DIFF-TRFA";
	
	
	this.table["DISPO-TRCO"]={};
	this.table["DISPO-TRCO"]["100"]="DIFF-ASFA";
	this.table["DISPO-TRCO"]["1000"]="DIFF-FACI";
	this.table["DISPO-TRCO"]["10000"]="DIFF-TRFA";
	this.table["DISPO-TRCO"]["10001"]="DIFF-REAU";
	
	
	this.table["DISPO-BANA"]={};
	this.table["DISPO-BANA"]["100"]="DIFF-FACI";
	this.table["DISPO-BANA"]["1000"]="DIFF-TRFA";
	this.table["DISPO-BANA"]["10000"]="DIFF-REAU";
	this.table["DISPO-BANA"]["10001"]="DIFF-REAU";
	
	
	
	this.disponibilities=["DISPO-TRRA","DISPO-RARE","DISPO-INHA","DISPO-ASCO","DISPO-COUR","DISPO-TRCO","DISPO-BANA"];
	
	
	this.getDifficulteTestDispo = function(disponibilityId,qualiteId,nombreHabitant){
		var dispo = this.adaptDispoParQualite(disponibilityId,qualiteId);
		return this.getDifficultePourDispo(dispo,nombreHabitant);
		
	};
	
	this.getIndexDisponibilte=function(disponibilityId){
		var index = -1;
		for(var i=0; i<this.disponibilities.length;i++){
			if( disponibilityId === this.disponibilities[i]){
				index=i;
			}
		}
		return index;
	};
	
	this.adaptDispoParQualite= function(disponibilityId,qualiteId){

		var index = this.getIndexDisponibilte(disponibilityId);
		switch(qualiteId){
			case "QUAL-EXCE": index -=2;break;
			case "QUAL-BONN": index -=1;break;
			case "QUAL-MEDI": index +=1;break;

		}
		
		if(index <0){
			index =0;
		}
		if(index >= this.disponibilities.length){
			index=this.disponibilities.length-1;
		}
		return this.disponibilities[index];
	};
	
	this.getDifficultePourDispo=function(disponibilityId,nombreHabitant){
		var nombreHabitantIndice = "";
		
		switch(true){
			case (nombreHabitant <= 100 ): nombreHabitantIndice="100";break;
			case (nombreHabitant<= 1000 && nombreHabitant>100): nombreHabitantIndice="1000";break;
			case (nombreHabitant<= 10000 && nombreHabitant>1000): nombreHabitantIndice="10000";break;
			case (nombreHabitant> 10000): nombreHabitantIndice="10001";break;
		}
		return this.table[disponibilityId][nombreHabitantIndice];
	};
}
