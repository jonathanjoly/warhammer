'use strict';

describe('Controller: TalentsCtrl', function () {

  // load the controller's module
  beforeEach(module('warhammerApp'));

  var TalentsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TalentsCtrl = $controller('TalentsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(TalentsCtrl.awesomeThings.length).toBe(3);
  });
});
