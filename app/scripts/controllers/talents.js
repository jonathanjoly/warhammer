'use strict';

/**
 * @ngdoc function
 * @name warhammerApp.controller:TalentsCtrl
 * @description
 * # TalentsCtrl
 * Controller of the warhammerApp
 */
angular.module('warhammerApp')
  .controller('TalentsCtrl', ['$scope','dataSet', function ($scope,dataSet) {
	  $scope.dataSet = dataSet;
	  $scope.selectedObject = {};
	  
	  
	  $scope.isActive=function(object){
		  return (object.id === $scope.selectedObject.id ? 'active':'');
	  };
	  
	  $scope.set=function(object){
		  $scope.selectedObject = object; 
	  };
  }]);
