'use strict';

/**
 * @ngdoc service
 * @name warhammerApp.dataSet
 * @description
 * # dataSet
 * Factory in the warhammerApp.
 */
angular.module('warhammerApp')
  .factory('dataSet',['$http', function ($http) {
	   var dataSet = {data:{},index:{}};
	   
	   var apis = [
	            'actions',
	              'weapons',
	              'armors',
	              'attributes',
	              'carrers',
	              'skills',
	              'disponibilities',
	              'magics',
	              'objects',
	              'talents',
	              'races',
	              'sources',
	              'areas'
	   ];
	   
	   var get = function(module){
		   $http({method: 'GET', url: 'api/'+module+'.json',cache:true, params: {module : module}}).then(
			function(data){
				 dataSet.data[module] = data.data;
				  
				  for (var i=0;i<data.data.length;i++){
					  dataSet.index[data.data[i].id]=data.data[i];
				   }
			}	   
		   
		   );
		   
			
	   };
	   
	   
	   
	   for(var i=0; i< apis.length;i++){
		   get(apis[i]);
	   }
		
	   $http({method: 'GET', url: 'api/bonusTalents.json',cache:true}).then(
		function(data){
			 dataSet.data['bonusTalents'] = data.data;
		}	   
	   
	   );
	   	   
	   dataSet.index["cc"]={nom:'Capacité de combat'};
	   dataSet.index["ct"]={nom:'Capacité de tir'};
	   dataSet.index["f"]={nom:'Force'};
	   dataSet.index["e"]={nom:'Endurance'};
	   dataSet.index["ag"]={nom:'Agilité'};
	   
	   dataSet.index["int"]={nom:'Intelligence'};
	   dataSet.index["fm"]={nom:'Force mentale'};
	   dataSet.index["soc"]={nom:'Sociabilité'};
	   dataSet.index["a"]={nom:'Attaque'};
	   dataSet.index["b"]={nom:'Blessure'};
		   
	   dataSet.index["m"]={nom:'Mouvement'};
	   dataSet.index["mag"]={nom:'Magie'};
		   
	   
	   return dataSet;
  
  }]);
