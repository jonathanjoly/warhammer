def getMappingSheets():
    dict = {}
    dict['Zone'] = 'areas'
    dict['Dispo'] = 'disponibilities'
    dict['Objet'] = 'objects'
    dict['Attribut'] = 'attributes'
    dict['Armure'] = 'armors'
    dict['Magie'] = 'magics'
    dict['Races'] = 'races'
    dict['Source'] = 'sources'
    dict['Action'] = 'actions'

    
    return dict

def getMappingFields():
    dict = {}
    dict['SID'] = 'sid'
    dict['ID'] = 'id'
    dict['NOM'] = 'nom'
    dict['TYPE'] = 'type'
    dict['PRIX'] = 'prix'
    dict['ENCOMBREMENT'] = 'encombrement'
    dict['DISPONIBILITE'] = 'disponobilite'
    dict['SOURCE'] = 'source'
    dict['DIFFICULTE'] = 'difficulte'
    dict['INGREDIENT'] = 'ingredient'
    dict['BONUS'] = 'bonus'
    dict['TEMPS'] = 'temps'
    dict['GROUPE'] = 'groupe'
    dict['DEGAT'] = 'degat'
    dict['RECHARGE'] = 'recharge'
    dict['PORTEE'] = 'portee'
    dict['DESCRIPTION'] = 'description'
    dict['ZONE'] = 'zone'
    dict['PA'] = 'pa'
    dict['NOTE'] = 'note'
    dict['BASE'] = 'base'
    dict['HISTORIQUE'] = 'historique'
    dict['CONSEIL'] = 'conseil'
    dict['CARACTERISTIQUE'] = 'caracteristique'
    dict['CC'] = 'cc'
    dict['CT'] = 'ct'
    dict['F'] = 'f'
    dict['E'] = 'e'
    dict['AG'] = 'ag'
    dict['INT'] = 'int'
    dict['FM'] = 'fm'
    dict['SOC'] = 'soc'
    dict['A'] = 'a'
    dict['B'] = 'b'
    dict['M'] = 'm'
    dict['MAG'] = 'mag'
    
   
    return dict
        
    
    
    
    












