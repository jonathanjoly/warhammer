'use strict';

describe('Controller: MagicsCtrl', function () {

  // load the controller's module
  beforeEach(module('warhammerApp'));

  var MagicsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MagicsCtrl = $controller('MagicsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MagicsCtrl.awesomeThings.length).toBe(3);
  });
});
