'use strict';

/**
 * @ngdoc function
 * @name warhammerApp.controller:AttributsCtrl
 * @description
 * # AttributsCtrl
 * Controller of the warhammerApp
 */

 angular.module('warhammerApp')
   .controller('AttributesCtrl',['$scope','dataSet', function ($scope,dataSet) {
 	  $scope.dataSet = dataSet;
 	  $scope.selectedObject = {};


 	  $scope.isActive=function(object){
 		  return (object.id === $scope.selectedObject.id ? 'active':'');
 	  };

 	  $scope.set=function(object){
 		  $scope.selectedObject = object;
 	  };
  }]);
