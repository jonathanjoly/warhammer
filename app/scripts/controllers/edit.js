'use strict';

/**
 * @ngdoc function
 * @name warhammerApp.controller:EditCtrl
 * @description
 * # EditCtrl
 * Controller of the warhammerApp
 */
angular.module('warhammerApp')
  .controller('EditCtrl',['$scope','session','dataSet',function ($scope,session,dataSet) {
	  $scope.character = session.selectedCharacter;
	  $scope.dataSet = dataSet;
	    
	  
	  $scope.skills=[];
	 
	  
	  
	  for(var key in $scope.character.competences){
		  
		  var skill = $scope.character.competences[key];
		  
		  var record = skill;
		  record.name = $scope.dataSet.index[key].nom;
		  record.additionalBonus = 0;
		  record.caract = $scope.dataSet.index[key].caracteristique;

		  
		  
		  
		  for(var i=0;i< $scope.character.talents.length;i++){
			  var talent = $scope.character.talents[i];
			  
			  if(talent in  $scope.dataSet.data.bonusTalents){ 
				  var competences = $scope.dataSet.data.bonusTalents[talent];
				  for(var j=0; j<competences.length;j++){
					  if(competences[j].competence === key){

						  if('condition' in competences[j]){
							  
							  var record2 = record;
							  record2.name = record2.name +' '+ competences[j].condition;
							  record2.additionalBonus = competences[j].bonus;
							  
							  if(notInSkills(record2.name)){
								  $scope.skills.push(record2);
							  }
							  
						  }
						  else{
							  record.additionalBonus = competences[j].bonus;
						  }
						  
					  }
				  }
			  }
		  }
		  
		  $scope.skills.push(record); 
		 
		  
	  }
	  
	  function notInSkills(skillName){
		  var exist= true;
		  
		  for(var i=0;i<$scope.skills.length;i++){
			  if($scope.skills.name === skillName){
				  exist = false;
			  }
		  }
		  return exist;
		  
	  }
	  
	  console.log($scope.skills);
	  
	  
	  
	/*  
	  for(var i=0;i< $scope.character.talents.length;i++){
		
		  
		  
		  var talent = $scope.character.talents[i];
		  console.log(talent);
		  
		  //var skill = $scope.character.competences;
		  
		  if(talent in  $scope.dataSet.data.bonusTalents){ 
			  var competences = $scope.dataSet.data.bonusTalents[talent];
			  console.log(competences);
			  
			  
			  for(var j=0; j<competences.length;j++){
				  // competence
				  
				  
				  
				  if(competences[j].competence in $scope.character.competences){
					  
					  if('condition' in competences[j]){
						  
					  }
					  else{
						  
					  }
					  console.log($scope.character.competences[competences[j].competence]);
				  }
				  
				  
			  }
			  
		  }
		  
	  }*/
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
		$scope.tableToExcel = (function() {
			  var uri = 'data:application/vnd.ms-excel;base64,'
			    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
			    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
			    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
			  return function(table, name) {
			    if (!table.nodeType) table = document.getElementById(table)
			    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
			    window.location.href = uri + base64(format(template, ctx))
			  }
		})();
  
  }]);
