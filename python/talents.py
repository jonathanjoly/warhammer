import json

def generateTalents(workbook,fieldDict):
    
    talentCompetence = {}
    
    sheet= workbook.sheet_by_name('CompetenceTalent')
    
    for rowIndex in range(1,sheet.nrows):
        competence = sheet.cell(rowIndex,1).value 
        talent = sheet.cell(rowIndex,2).value
        
        if not talent in talentCompetence:
            talentCompetence[talent]=[]
        
        talentCompetence[talent].append(competence)
    
    
    sheet= workbook.sheet_by_name('Talents')
    list = []
     
    #LOOP ON ROW 
    for rowIndex in range(1,sheet.nrows):
    
        object = {}
    
        #LOOP ON COLS 
        for colIndex in range(sheet.ncols):    
            columnName = sheet.cell(0,colIndex).value      
            object[fieldDict[columnName]]= sheet.cell(rowIndex,colIndex).value
            
        id = object['id']
        
        if id in talentCompetence:
            object['competences'] = talentCompetence[id]
            
        list.append(object)
    
    with open('../app/api/talents.json', 'w') as f:
        json.dump(list, f)
        