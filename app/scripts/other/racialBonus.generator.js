'use strict';
function GenerateRacialBonus(){
	
	this.getTalentChoices= function(raceId){
		var talentChoices =[];
		var generateTalent = new GenerateTalent();
		
		switch(raceId){
		
			case "RACE-ELFE": 
				talentChoices = [{selection:"TALE-ACVI",choice:["TALE-ACVI"]},{selection:"TALE-HAAE",choice:["TALE-HAAE","TALE-MAAL"]},{selection:"TALE-INTL",choice:["TALE-INTL","TALE-SAFR"]},{selection:"TALE-VINO",choice:["TALE-VINO"]}];
				break;
			case "RACE-HALF": 
				
				var talent = generateTalent.generate(raceId);
				talentChoices =[{selection:"TALE-MALP",choice:["TALE-MALP"]},{selection:"TALE-RECH",choice:["TALE-RECH"]},{selection:"TALE-VINO",choice:["TALE-VINO"]},{selection:talent,choice:[talent]}];
				break;
			case "RACE-HUMA": 
				var talent1 = generateTalent.generate(raceId);
				var talent2 ="";
				do{
					talent2 = generateTalent.generate(raceId);
				}
				while(talent1 === talent2);
				
				talentChoices =[{selection:talent1,choice:[talent1]},{selection:talent2,choice:[talent2]}];	
				break;
			case "RACE-NAIN": 
				talentChoices = [{selection:"TALE-FUVE",choice:["TALE-FUVE"]},{selection:"TALE-REMA",choice:["TALE-REMA"]},{selection:"TALE-ROBU",choice:["TALE-ROBU"]},{selection:"TALE-SAFN",choice:["TALE-SAFN"]},{selection:"TALE-VALE",choice:["TALE-VALE"]},{selection:"TALE-VINO",choice:["TALE-VINO"]}];
				break;
		}
		return talentChoices;
	};
	
	
	this.getSkillChoices= function(raceId){
		var skillChoice =[];
		
		switch(raceId){
		
			case "RACE-ELFE": 
				skillChoice = [{selection:"COMP-LAEL",choice:["COMP-LAEL"]},{selection:"COMP-LARE",choice:["COMP-LARE"]},{selection:"COMP-CGEL",choice:["COMP-CGEL"]}];
				break;
				
			case "RACE-HALF": 
				skillChoice = [{selection:"COMP-COME",choice:["COMP-COME"]},{selection:"COMP-COGH",choice:["COMP-COGH"]},{selection:"COMP-CGHA",choice:["COMP-CGHA"]},{selection:"COMP-LAHA",choice:["COMP-LAHA"]},{selection:"COMP-LARE",choice:["COMP-LARE"]},{selection:"COMP-MECU",choice:["COMP-MECU","COMP-MEFE"]}];
				break;
				
			case "RACE-HUMA": 
				skillChoice = [{selection:"COMP-COME",choice:["COMP-COME"]},{selection:"COMP-LARE",choice:["COMP-LARE"]},{selection:"COMP-CGEM",choice:["COMP-CGEM"]}];
				break;
			case "RACE-NAIN": 
				skillChoice = [{selection:"COMP-CGNA",choice:["COMP-CGNA"]},{selection:"COMP-LAKH",choice:["COMP-LAKH"]},{selection:"COMP-LARE",choice:["COMP-LARE"]},{selection:"COMP-MEFO",choice:["COMP-MEFO","COMP-MEMA","COMP-MEMI"]}];
				break;
		}

				

		return skillChoice;
	};
}