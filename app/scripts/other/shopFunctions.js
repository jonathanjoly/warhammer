'use strict';
function objectFilter(list,type){
	var objects=[];
	for(var i=0;i<list.length;i++){
		if(list[i].type === type){
			objects.push(list[i]);
		}
	}
	return objects;
}
/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
	
function objectInCart(cart,nom,qualityId){

	var index =-1;
	for(var i=0;i<cart.length;i++){

		if(cart[i].nom ===nom && cart[i].quality.id ===qualityId){
			index= i;
		}
	}
	return index;
}
/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
	
function computePrice(price,qualityId,quantity,isStruct){
	

	
	var moneyObject = new Money();

	
	var money = price;

		
	if(!isStruct){
		money=moneyObject.stringToMoney(money);	

	}
	
	
	money = moneyObject.adaptpriceQuality(money,qualityId);
	
	money = moneyObject.multiply(money,quantity);

	money.descr = moneyObject.toString(money);
	
	return money;
}
/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
	
function adaptValueWithGossip(value,gossipId){
	switch(gossipId){
		case "SC": value= Math.round(value/2); break;
		case "C1": value= value +10; break;
		case "C2": value= value +20; break;
	}
	return value;
}
/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
	
function adaptValueWithPopulationRarety(value,disponibiliteId,qualiteId,numberOfResident){
	var disponibilityObjet = new Disponibility();
	
	switch(disponibilityObjet.getDifficulteTestDispo(disponibiliteId,qualiteId,numberOfResident)){
		case"DIFF-MAJE": value = "À la discrétion du MJ";break;
		case"DIFF-TRDI": value= value -30;break;
		case"DIFF-DIFF": value= value -20;break;
		case"DIFF-ASDI": value= value -10;break;
		case"DIFF-ASFA": value=  value +10;break;
		case"DIFF-FACI": value=  value +20;break;
		case"DIFF-TRFA": value=  value +30;break;
		case"DIFF-REAU": value = "Réussite automatique";break;
	}
	
	return value;
}