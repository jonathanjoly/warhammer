'use strict';
function PhysicalTrait(){
	
	this.cheveux ="";
	this.poids=0;
	this.taille=0;
	this.yeux="";
	this.traits="";
	
	this.get=function(traitId){
		var trait="";
		
		switch(traitId){
			case "CHEVEUX": trait=this.cheveux;break;
			case "POIDS": trait=this.poids;break;
			case "YEUX": trait=this.yeux;break;
			case "TAILLE": trait=this.taille;break;
			case "SIGNE": trait=this.traits;break;
		}
		
		return trait;
	};
	
	this.set=function(traitId,trait){
	
		
		switch(traitId){
			case "CHEVEUX": this.cheveux=trait;break;
			case "POIDS": this.poids=trait;break;
			case "YEUX": this.yeux=trait;break;
			case "TAILLE": this.taille=trait;break;
			case "SIGNE": this.traits=trait;break;
		}
		

	};
	
}

