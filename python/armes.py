import json

def generateArmes(workbook,fieldDict):
    
    armesTalent = {}
    
    sheet= workbook.sheet_by_name('ArmesAttribut')
    
    for rowIndex in range(1,sheet.nrows):
        arme = sheet.cell(rowIndex,1).value 
        attribut = sheet.cell(rowIndex,2).value
        
        if not arme in armesTalent:
            armesTalent[arme]=[]
        
        armesTalent[arme].append(attribut)
    
    
    sheet= workbook.sheet_by_name('Armes')
    list = []
     
    #LOOP ON ROW 
    for rowIndex in range(1,sheet.nrows):
    
        object = {}
    
        #LOOP ON COLS 
        for colIndex in range(sheet.ncols):    
            columnName = sheet.cell(0,colIndex).value      
            object[fieldDict[columnName]]= sheet.cell(rowIndex,colIndex).value
            
        id = object['id']
        
        if id in armesTalent:
            object['attributs'] = armesTalent[id]
            
        list.append(object)
    
    with open('../app/api/weapons.json', 'w') as f:
        json.dump(list, f)
        