'use strict';

/**
 * @ngdoc service
 * @name warhammerApp.session
 * @description
 * # session
 * Factory in the warhammerApp.
 */
angular.module('warhammerApp')
  .factory('session',[ function () {
	  
	  var session={};

	  session.selectedCharacter = {};

    // Public API here
    return session;
    
  }]);
