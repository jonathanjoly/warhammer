'use strict';

describe('Controller: ArmorsCtrl', function () {

  // load the controller's module
  beforeEach(module('warhammerApp'));

  var ArmorsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ArmorsCtrl = $controller('ArmorsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ArmorsCtrl.awesomeThings.length).toBe(3);
  });
});
