'use strict';

/**
 * @ngdoc function
 * @name warhammerApp.controller:TimeforrunesCtrl
 * @description
 * # TimeforrunesCtrl
 * Controller of the warhammerApp
 */
angular.module('warhammerApp')
  .controller('TimeforrunesCtrl',['$scope', function ($scope) {
  
	// PARAMETRE PAR DEFAUT
		 $scope.runeType = 'TMP';
		 $scope.FMbonus = '0';
		 $scope.numberMagicPoint = 1;
		 
		 var dice = new Dice();
		 

		function minuteToHour(minutes){
			var rest = minutes%60;
			var hour = (minutes-rest)/60;
			
			return {hours:hour,minutes: rest};
		}
		
		// LANCER POUR CREATION  
		function launchCreation(numberMagicPoint){
			
			var diceResult=0;
			
			for(var i=0; i<numberMagicPoint;i++){
				diceResult+=dice.D10();
			}
			
			return diceResult;
		}
		
		// FUNCTION POUR RUNE PERMANENTE ------------------------------------------------------------------------------------------------------------
		
		function permanentRune(numberMagicPoint,difficulty,FM,activation){
			
			var weekNumber = permanentWritting(numberMagicPoint,difficulty);
			var monthNumber = permanentActivation(FM,activation);
			var dayNumber = dice.D10();
			
			return {weekNumber:weekNumber,monthNumber:monthNumber,dayNumber:dayNumber}
		}
		
		function permanentWritting(numberMagicPoint,difficulty){
			var weekNumber = 0;
			
			do{
				weekNumber++;		
			}while(launchCreation(numberMagicPoint) > difficulty);
			
			return weekNumber;
		}
		
		function permanentActivation(FM,activation){
			
			var successNumber = 0;
			var monthNumber =0;
			
			do{
				monthNumber++;
				
				if(dice.D100()<=FM){
					successNumber++;
				}
				
			}while(successNumber < activation)
			return monthNumber;
		}
		
		
		// FUNCTION POUR RUNE TEMPORAIRE ------------------------------------------------------------------------------------------------------------
		function doTemporaryRune(numberMagicPoint,difficulty,FM,activation,number){
			
			var minuteNumber=0;
			
			for(var i=0;i<number;i++){
				minuteNumber+=temporaryRune(numberMagicPoint,difficulty,FM,activation);
			}
			
			
			
			return minuteNumber;
		}

		function temporaryRune(numberMagicPoint,difficulty,FM,activation){
			
			var minuteNumber = temporaryWritting(numberMagicPoint,difficulty);
			minuteNumber += temporaryActivation(FM,activation);
			minuteNumber += dice.D10();
			
			return minuteNumber;
		}
		
		function temporaryWritting(numberMagicPoint,difficulty){
			var minuteNumber = 0;
			
			do{
				minuteNumber+= dice.D10()+dice.D10();		
			}while(launchCreation(numberMagicPoint) > difficulty);
			
			return minuteNumber;
		}
		
		function temporaryActivation(FM,activation){
			
			var successNumber = 0;
			var minuteNumber =0;
			
			do{
				minuteNumber+=20;
				
				if(dice.D100()<=FM){
					successNumber++;
				}
				
			}while(successNumber < activation)
			return minuteNumber;
		}
		
		$scope.compute = function(){
			
			var numberMagicPoint  = $scope.numberMagicPoint;
			var difficulty = $scope.difficulty;
			var	activation = $scope.activation;
			var number = $scope.numberRune;
			var FM  = $scope.FMbase +  $scope.FMbonus;
			
			
			if('TMP' === $scope.runeType){
				$scope.temporaryTime=minuteToHour(doTemporaryRune(numberMagicPoint,difficulty,FM,activation,number));
			}
			else{
				$scope.tempsPermanante=permanentRune(numberMagicPoint,difficulty,FM,activation);
			}
			
		};
		
		$scope.visible=function(type){
			return type === $scope.runeType ? '':'hidden';
		}; 
	  
	  
	  
	  
  }]);
