'use strict';

/**
 * @ngdoc function
 * @name warhammerApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the warhammerApp
 */
angular.module('warhammerApp')
  .controller('HomeCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
