'use strict';
//Livre de base table 2-11, 2-12, 2-13, 2-14, 2-15 , 2-16, 2-17
function GenerateOrigin(){
	
	this.table=[];
	this.table["ASTRAL"] = [ "Wymund l'Anachorète", "Wymund l'Anachorète", "Wymund l'Anachorète", "Wymund l'Anachorète", "Wymund l'Anachorète", "La Grande Croix", "La Grande Croix", "La Grande Croix", "La Grande Croix", "La Grande Croix", "Le Trait du Peintre", "Le Trait du Peintre", "Le Trait du Peintre", "Le Trait du Peintre", "Le Trait du Peintre", "Gnuthus le Buffle", "Gnuthus le Buffle", "Gnuthus le Buffle", "Gnuthus le Buffle", "Gnuthus le Buffle", "Gnuthus le Buffle", "Gnuthus le Buffle", "Gnuthus le Buffle", "Gnuthus le Buffle", "Gnuthus le Buffle", "Dragomas le Dragon", "Dragomas le Dragon", "Dragomas le Dragon", "Dragomas le Dragon", "Dragomas le Dragon", "Le Crépuscule", "Le Crépuscule", "Le Crépuscule", "Le Crépuscule", "Le Crépuscule", "Le Fourreau de Grungni", "Le Fourreau de Grungni", "Le Fourreau de Grungni", "Le Fourreau de Grungni", "Le Fourreau de Grungni", "Mammit le Sage", "Mammit le Sage", "Mammit le Sage", "Mammit le Sage", "Mammit le Sage", "Mummit le Fou", "Mummit le Fou", "Mummit le Fou", "Mummit le Fou", "Mummit le Fou", "Les Diceux Boeufs", "Les Diceux Boeufs", "Les Diceux Boeufs", "Les Diceux Boeufs", "Les Diceux Boeufs", "Le Danseur", "Le Danseur", "Le Danseur", "Le Danseur", "Le Danseur", "Le Tambour", "Le Tambour", "Le Tambour", "Le Tambour", "Le Tambour", "Le Flétiste", "Le Flétiste", "Le Flétiste", "Le Flétiste", "Le Flétiste", "Vobist le Péle", "Vobist le Péle", "Vobist le Péle", "Vobist le Péle", "Vobist le Péle", "La Charrette Brisée", "La Charrette Brisée", "La Charrette Brisée", "La Charrette Brisée", "La Charrette Brisée", "La Chévre Sauvage", "La Chévre Sauvage", "La Chévre Sauvage", "La Chévre Sauvage", "La Chévre Sauvage", "Le Chaudron de Rhya", "Le Chaudron de Rhya", "Le Chaudron de Rhya", "Le Chaudron de Rhya", "Le Chaudron de Rhya", "Cackelfax le Coq", "Cackelfax le Coq", "Cackelfax le Coq", "Cackelfax le Coq", "Cackelfax le Coq", "Le Grimoire", "Le Grimoire", "Le Grimoire", "L'étoile du Sorcier", "L'étoile du Sorcier"];
	this.table["LIEU"] = {};
	this.table["LIEU"]["TYPE"] = ["Cité", "Ville prospère", "Bourg", "Ville fortifiée", "Village agricole", "Village pauvre", "Petite communauté", "Ferme (élevage)", "Ferme (culture)", "Taudis"];
	this.table["LIEU"]["REGION"] = [ "Averland",   "Hochland", "Middenland",   "Nordland",  "Ostermark",   "Ostland",   "Reikland",   "Stirland",  "Talabecland",  "Wissenland"];   
	
	this.table["RACE-ELFE"]={};
	this.table["RACE-ELFE"]["FAMILLE"] = ["0","1","1","1","1","2","2","2","2","3"];
	this.table["RACE-ELFE"]["NAISSANCE"] = ["Cité dAltdorf","Cité dAltdorf","Cité dAltdorf","Cité dAltdorf","Cité dAltdorf","Cité dAltdorf","Cité dAltdorf","Cité dAltdorf","Cité dAltdorf","Cité dAltdorf","Cité dAltdorf","Cité dAltdorf","Cité dAltdorf","Cité dAltdorf","Cité dAltdorf","Cité dAltdorf","Cité dAltdorf","Cité dAltdorf","Cité dAltdorf","Cité dAltdorf","Cité de Marienburg","Cité de Marienburg","Cité de Marienburg","Cité de Marienburg","Cité de Marienburg","Cité de Marienburg","Cité de Marienburg","Cité de Marienburg","Cité de Marienburg","Cité de Marienburg","Cité de Marienburg","Cité de Marienburg","Cité de Marienburg","Cité de Marienburg","Cité de Marienburg","Cité de Marienburg","Cité de Marienburg","Cité de Marienburg","Cité de Marienburg","Cité de Marienburg","Forêt de Laurelorn","Forêt de Laurelorn","Forêt de Laurelorn","Forêt de Laurelorn","Forêt de Laurelorn","Forêt de Laurelorn","Forêt de Laurelorn","Forêt de Laurelorn","Forêt de Laurelorn","Forêt de Laurelorn","Forêt de Laurelorn","Forêt de Laurelorn","Forêt de Laurelorn","Forêt de Laurelorn","Forêt de Laurelorn","Forêt de Laurelorn","Forêt de Laurelorn","Forêt de Laurelorn","Forêt de Laurelorn","Forêt de Laurelorn","Forêt de Laurelorn","Forêt de Laurelorn","Forêt de Laurelorn","Forêt de Laurelorn","Forêt de Laurelorn","Forêt de Laurelorn","Forêt de Laurelorn","Forêt de Laurelorn","Forêt de Laurelorn","Forêt de Laurelorn","Grande Forêt","Grande Forêt","Grande Forêt","Grande Forêt","Grande Forêt","Grande Forêt","Grande Forêt","Grande Forêt","Grande Forêt","Grande Forêt","Grande Forêt","Grande Forêt","Grande Forêt","Grande Forêt","Grande Forêt","Forêt de Reikwald","Forêt de Reikwald","Forêt de Reikwald","Forêt de Reikwald","Forêt de Reikwald","Forêt de Reikwald","Forêt de Reikwald","Forêt de Reikwald","Forêt de Reikwald","Forêt de Reikwald","Forêt de Reikwald","Forêt de Reikwald","Forêt de Reikwald","Forêt de Reikwald","Forêt de Reikwald"];
	this.table["RACE-ELFE"]["AGE"] = ["30","30","30","30","30","35","35","35","35","35","40","40","40","40","40","45","45","45","45","45","50","50","50","50","50","55","55","55","55","55","60","60","60","60","60","65","65","65","65","65","70","70","70","70","70","75","75","75","75","75","80","80","80","80","80","85","85","85","85","85","90","90","90","90","90","95","95","95","95","95","100","100","100","100","100","105","105","105","105","105","110","110","110","110","110","115","115","115","115","115","120","120","120","120","120","125","125","125","125","125"];
	
	this.table["RACE-HALF"]={};
	this.table["RACE-HALF"]["AGE"] = ["20","20","20","20","20","22","22","22","22","22","24","24","24","24","24","26","26","26","26","26","28","28","28","28","28","30","30","30","30","30","32","32","32","32","32","34","34","34","34","34","36","36","36","36","36","38","38","38","38","38","40","40","40","40","40","42","42","42","42","42","44","44","44","44","44","46","46","46","46","46","50","50","50","50","50","52","52","52","52","52","54","54","54","54","54","56","56","56","56","56","58","58","58","58","58","60","60","60","60","60"];
	this.table["RACE-HALF"]["FAMILLE"] = ["1","2","2","3","3","4","4","5","5","6"];
	this.table["RACE-HALF"]["NAISSANCE"] = ["Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","Le Moot","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""];
	
	this.table["RACE-HUMA"]={};
	this.table["RACE-HUMA"]["AGE"] = ["16","16","16","16","16","17","17","17","17","17","18","18","18","18","18","19","19","19","19","19","20","20","20","20","20","21","21","21","21","21","22","22","22","22","22","23","23","23","23","23","24","24","24","24","24","25","25","25","25","25","26","26","26","26","26","27","27","27","27","27","28","28","28","28","28","29","29","29","29","29","30","30","30","30","30","31","31","31","31","31","32","32","32","32","32","33","33","33","33","33","34","34","34","34","34","35","35","35","35","35"];
	this.table["RACE-HUMA"]["FAMILLE"] = ["0","1","1","2","2","3","3","4","4","5"];
	this.table["RACE-HUMA"]["NAISSANCE"] = ["","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""];
	
	this.table["RACE-NAIN"]={};
	this.table["RACE-NAIN"]["AGE"] = ["20","20","20","20","20","25","25","25","25","25","30","30","30","30","30","35","35","35","35","35","40","40","40","40","40","45","45","45","45","45","50","50","50","50","50","55","55","55","55","55","60","60","60","60","60","65","65","65","65","65","70","70","70","70","70","75","75","75","75","75","80","80","80","80","80","85","85","85","85","85","90","90","90","90","90","95","95","95","95","95","100","100","100","100","100","105","105","105","105","105","110","110","110","110","110","115","115","115","115","115"];
	this.table["RACE-NAIN"]["FAMILLE"] = ["0","0","0","1","1","1","1","2","2","3"];
	this.table["RACE-NAIN"]["NAISSANCE"] = ["","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","Karak Norn (Montagnes Grises)","Karak Norn (Montagnes Grises)","Karak Norn (Montagnes Grises)","Karak Norn (Montagnes Grises)","Karak Norn (Montagnes Grises)","Karak Norn (Montagnes Grises)","Karak Norn (Montagnes Grises)","Karak Norn (Montagnes Grises)","Karak Norn (Montagnes Grises)","Karak Norn (Montagnes Grises)","Karak Izor (la VoÃ»te)","Karak Izor (la VoÃ»te)","Karak Izor (la VoÃ»te)","Karak Izor (la VoÃ»te)","Karak Izor (la VoÃ»te)","Karak Izor (la VoÃ»te)","Karak Izor (la VoÃ»te)","Karak Izor (la VoÃ»te)","Karak Izor (la VoÃ»te)","Karak Izor (la VoÃ»te)","Karak Hirn (Montagnes Noires)","Karak Hirn (Montagnes Noires)","Karak Hirn (Montagnes Noires)","Karak Hirn (Montagnes Noires)","Karak Hirn (Montagnes Noires)","Karak Hirn (Montagnes Noires)","Karak Hirn (Montagnes Noires)","Karak Hirn (Montagnes Noires)","Karak Hirn (Montagnes Noires)","Karak Hirn (Montagnes Noires)","Karak Kadrin (Montagnes du Bord du Monde)","Karak Kadrin (Montagnes du Bord du Monde)","Karak Kadrin (Montagnes du Bord du Monde)","Karak Kadrin (Montagnes du Bord du Monde)","Karak Kadrin (Montagnes du Bord du Monde)","Karak Kadrin (Montagnes du Bord du Monde)","Karak Kadrin (Montagnes du Bord du Monde)","Karak Kadrin (Montagnes du Bord du Monde)","Karak Kadrin (Montagnes du Bord du Monde)","Karak Kadrin (Montagnes du Bord du Monde)","Karaz-a-Karak (Montagnes du Bord du Monde)","Karaz-a-Karak (Montagnes du Bord du Monde)","Karaz-a-Karak (Montagnes du Bord du Monde)","Karaz-a-Karak (Montagnes du Bord du Monde)","Karaz-a-Karak (Montagnes du Bord du Monde)","Karaz-a-Karak (Montagnes du Bord du Monde)","Karaz-a-Karak (Montagnes du Bord du Monde)","Karaz-a-Karak (Montagnes du Bord du Monde)","Karaz-a-Karak (Montagnes du Bord du Monde)","Karaz-a-Karak (Montagnes du Bord du Monde)","Karaz-a-Karak (Montagnes du Bord du Monde)","Karaz-a-Karak (Montagnes du Bord du Monde)","Karaz-a-Karak (Montagnes du Bord du Monde)","Karaz-a-Karak (Montagnes du Bord du Monde)","Karaz-a-Karak (Montagnes du Bord du Monde)","Karaz-a-Karak (Montagnes du Bord du Monde)","Karaz-a-Karak (Montagnes du Bord du Monde)","Karaz-a-Karak (Montagnes du Bord du Monde)","Karaz-a-Karak (Montagnes du Bord du Monde)","Karaz-a-Karak (Montagnes du Bord du Monde)","Barak Varr (Golfe Noir)","Barak Varr (Golfe Noir)","Barak Varr (Golfe Noir)","Barak Varr (Golfe Noir)","Barak Varr (Golfe Noir)","Barak Varr (Golfe Noir)","Barak Varr (Golfe Noir)","Barak Varr (Golfe Noir)","Barak Varr (Golfe Noir)","Barak Varr (Golfe Noir)"];
	
	
	
	this.generate=function(raceId,sexeId){
		var origin = new Origin();
		var dice = new Dice();

		origin.set("ASTRAL",this.DefineAstral(dice.D100()));
		origin.set("FAMILLE",this.define(raceId,"FAMILLE",dice.D10()));
		origin.set("AGE",this.define(raceId,"AGE",dice.D100()));
		origin.set("NAISSANCE",this.DefineTown(raceId,dice.D100()));
		
		
		return origin;
	};
	
	
	this.define= function(raceId,infoId,diceRes){
		return this.table[raceId][infoId][diceRes-1];
	};
	
	this.DefineAstral= function(diceRes){
		return this.table["ASTRAL"][diceRes-1];
	};
	
	this.DefineType= function(diceRes){
		return this.table["LIEU"]["TYPE"][diceRes-1];
	};
	
	this.DefineRegion= function(diceRes){
		return this.table["LIEU"]["REGION"][diceRes-1];
	};
	
	this.DefineTown= function(raceId,diceRes){
		var lieux = this.table[raceId]["NAISSANCE"][diceRes-1];
		
		if(lieux === ""){
			var dice = new Dice();
			lieux= this.DefineType(dice.D10())+"("+this.DefineRegion(dice.D10())+")";
		}
		
		return lieux;
	};
	
	
	
}