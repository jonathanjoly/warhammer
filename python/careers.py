import json

def generateCareers(workbook,fieldDict):
    
    ChoixTalent = {}
    
    sheet= workbook.sheet_by_name('ChoixTalent')
    
    for rowIndex in range(1,sheet.nrows):
        choix = sheet.cell(rowIndex,1).value 
        talent = sheet.cell(rowIndex,2).value
        
        if not choix in ChoixTalent:
            ChoixTalent[choix]=[]
        
        ChoixTalent[choix].append(talent)
    
    sheet= workbook.sheet_by_name('CarriereChoixTalent')
    carriereChoixTalent = {}
    
    for rowIndex in range(1,sheet.nrows):
        id = sheet.cell(rowIndex,1).value 
        carriere = sheet.cell(rowIndex,2).value
        
        if not carriere in carriereChoixTalent:
            carriereChoixTalent[carriere]=[]
        
        carriereChoixTalent[carriere].append(ChoixTalent[id])

# ---------------------------------------------------------------------------------------------------------- 
# ---------------------------------------------------------------------------------------------------------- 
# ---------------------------------------------------------------------------------------------------------- 
# ---------------------------------------------------------------------------------------------------------- 
# ---------------------------------------------------------------------------------------------------------- 

    ChoixObjet= {}
    
    sheet= workbook.sheet_by_name('ChoixObjet')
    
    for rowIndex in range(1,sheet.nrows):
        choix = sheet.cell(rowIndex,1).value 
        objet ={}
        objet['id'] = sheet.cell(rowIndex,2).value
        objet['quantite'] = sheet.cell(rowIndex,3).value
        objet['qualite'] = sheet.cell(rowIndex,4).value
    
        
        if not choix in ChoixObjet:
            ChoixObjet[choix]=[]
        
        ChoixObjet[choix].append(objet)
    
    sheet= workbook.sheet_by_name('CarriereChoixObjet')
    carriereChoixObjet = {}
    
    for rowIndex in range(1,sheet.nrows):
        id = sheet.cell(rowIndex,1).value 
        carriere = sheet.cell(rowIndex,2).value
        
        if not carriere in carriereChoixObjet:
            carriereChoixObjet[carriere]=[]
        
        carriereChoixObjet[carriere].append(ChoixObjet[id])
 
# ---------------------------------------------------------------------------------------------------------- 
# ---------------------------------------------------------------------------------------------------------- 
# ---------------------------------------------------------------------------------------------------------- 
# ---------------------------------------------------------------------------------------------------------- 
# ---------------------------------------------------------------------------------------------------------- 

    ChoixCompetence= {}
    
    sheet= workbook.sheet_by_name('ChoixCompetence')
    
    for rowIndex in range(1,sheet.nrows):
        choix = sheet.cell(rowIndex,1).value 
        competence = sheet.cell(rowIndex,2).value
        
        if not choix in ChoixCompetence:
            ChoixCompetence[choix]=[]
        
        ChoixCompetence[choix].append(competence)
    
    sheet= workbook.sheet_by_name('CarriereChoixCompetence')
    carriereChoixCompetence = {}
    
    for rowIndex in range(1,sheet.nrows):
        id = sheet.cell(rowIndex,1).value 
        carriere = sheet.cell(rowIndex,2).value
        
        if not carriere in carriereChoixCompetence:
            carriereChoixCompetence[carriere]=[]
        
        carriereChoixCompetence[carriere].append(ChoixCompetence[id])
     
         
    # ---------------------------------------------------------------------------------------------------------- 
    # ---------------------------------------------------------------------------------------------------------- 
    # ---------------------------------------------------------------------------------------------------------- 
    # ---------------------------------------------------------------------------------------------------------- 
    # ---------------------------------------------------------------------------------------------------------- 
     
     
     
     
    sheet= workbook.sheet_by_name('Carrieres')
    list = []  
     
    #LOOP ON ROW 
    for rowIndex in range(1,sheet.nrows):
    
        object = {}
    
        #LOOP ON COLS 
        for colIndex in range(sheet.ncols):    
            columnName = sheet.cell(0,colIndex).value      
            object[fieldDict[columnName]]= sheet.cell(rowIndex,colIndex).value
            
        id = object['id']
        
        
        
        
        if id in carriereChoixTalent:
            object['talents'] = carriereChoixTalent[id]
             
            
        if id in carriereChoixCompetence:
            object['competences'] = carriereChoixCompetence[id]
            
        if id in carriereChoixObjet:
            object['objets'] = carriereChoixObjet[id]   
            
            
            
        list.append(object)
    
    with open('../app/api/carrers.json', 'w') as f:
        json.dump(list, f)
        