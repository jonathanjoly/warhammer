'use strict';
function Character()  {

		this.details ={};

		this.caractBase ={};
		this.caractPrise ={};

		this.carrieres=[];
		this.carriere ="";

		this.inventory = {};
		this.inventory.weapons =[];
		this.inventory.armors =[];
		this.inventory.objects = [];
		this.inventory.load = 0;

		this.purse = {co:0,pa:0,sc:0};

		this.armor = {head:0,leftArm:0,rightArm:0,leftLeg:0,rightLeg:0,body:0};

		this.hasAdditionalFortune = false;

		this.nouvelleCarriere=function(carriere){
			this.carrieres.push(carriere);
			this.carriere = carriere;

		};

		this.getMaxLoad=function(){
			var load =0;

			if(this.details.race === 'RACE-NAIN'){
				load = (this.caractBase.get('F') + this.caractPrise.get('F')) *20;
			}
			else{
				load = (this.caractBase.get('F') + this.caractPrise.get('F')) *10;
			}
			return load;

		};

		this.talents =[];

		this.ajoutTalent=function(talent){
			if(this.talents.indexOf(talent) === -1){
				this.talents.push(talent);

					switch(talent){
						case 'TALE-CHAN':
							this.hasAdditionalFortune = true;
							break;
						case 'TALE-COAP':
							this.caractBase.set('M',this.caractBase.get('M')+1);
							break;
						case 'TALE-DUCU':
							this.caractBase.set('B',this.caractBase.get('B')+1);
							break;
						case 'TALE-FOAC':
							this.caractBase.set('F',this.caractBase.get('F')+5);
							break;
						case 'TALE-GUNE':
							this.caractBase.set('CC',this.caractBase.get('CC')+5);
							break;
						case 'TALE-INTL':
							this.caractBase.set('INT',this.caractBase.get('INT')+5);
							break;
						case 'TALE-REEC':
							this.caractBase.set('AG',this.caractBase.get('AG')+5);
							break;
						case 'TALE-REAC':
							this.caractBase.set('E',this.caractBase.get('E')+5);
							break;
						case 'TALE-SAFR':
							this.caractBase.set('FM',this.caractBase.get('FM')+5);
							break;
						case 'TALE-SOCI':
							this.caractBase.set('SOC',this.caractBase.get('SOC')+5);
							break;
						case 'TALE-TIEL':
							this.caractBase.set('CT',this.caractBase.get('CT')+5);
							break;
					}
			}

		};

		this.competences ={};

		this.ajoutCompetence=function(competence,source){

			if(this.competences[competence]){ // If the skill already exist
				if(source !== this.competences[competence].derSource){ // If the source are diff
					if(this.competences[competence].bonus === 0){
						this.competences[competence].bonus = 10;
					}
					else{
						this.competences[competence].bonus =20;
					}
				}
			}
			else{
				this.competences[competence] = {bonus:0,derSource:source};
			}

		};

		this.addObject=function(object){

			var quantite = ''+object.quantite;
			var dIndex = quantite.indexOf("D");
			if(-1 < dIndex){
				var numberOfDice = quantite.substring(0,dIndex);
				var typeOfDice = quantite.substring(dIndex+1);
				var dice = new Dice();
				var res = 0;

				for(var i=0; i< numberOfDice;i++){
						if(typeOfDice > 10){
							res += dice.D100();
						}
						else{
							res += dice.D10();
						}
				}
				object.quantite = res;

			}

			if(object.load !== '-' && object.load !== '' && object.id !== 'OBEM-HARN' && object.id !== 'OBEM-SELL'){
				this.inventory.load += object.quantite * object.load;
			}

			this.inventory.objects.push(object);

		};
		this.addWeapon=function(object){

			if(object.load !== '-' && object.load !== ''){
				this.inventory.load += object.quantite * object.load;
			}
			this.inventory.weapons.push(object);
		};
		this.addArmor=function(object){

			switch (object.area) {
				case 'ZONE-TOUT':
						this.updateArmorPart('head',object.pa);
						this.updateArmorPart('body',object.pa);
						this.updateArmorPart('leftArm',object.pa);
						this.updateArmorPart('rightArm',object.pa);
						this.updateArmorPart('leftLeg',object.pa);
						this.updateArmorPart('rightLeg',object.pa);
					break;
				case 'ZONE-TETE':
					this.updateArmorPart('head',object.pa);
					break;
				case 'ZONE-CORP':
					this.updateArmorPart('body',object.pa);
					break;
				case 'ZONE-JAMB':
					this.updateArmorPart('leftLeg',object.pa);
					this.updateArmorPart('rightLeg',object.pa);
					break;
				case 'ZONE-COBA':
					this.updateArmorPart('body',object.pa);
					this.updateArmorPart('leftArm',object.pa);
					this.updateArmorPart('rightArm',object.pa)
					break;
				case 'ZONE-COJA':
					this.updateArmorPart('body',object.pa);
					this.updateArmorPart('leftLeg',object.pa);
					this.updateArmorPart('rightLeg',object.pa);
					break;
				case 'ZONE-CBJA':
					this.updateArmorPart('body',object.pa);
					this.updateArmorPart('leftArm',object.pa);
					this.updateArmorPart('rightArm',object.pa);
					this.updateArmorPart('leftLeg',object.pa);
					this.updateArmorPart('rightLeg',object.pa);
					break;
				case 'ZONE-BRAS':
					this.updateArmorPart('leftArm',object.pa);
					this.updateArmorPart('rightArm',object.pa);
					break;



			}

			if(object.load !== '-' && object.load !== ''){
				this.inventory.load += object.quantite * object.load;
			}
			this.inventory.armors.push(object);
		};

		this.updateArmorPart=function(part,point){
			if(this.armor[part]<point){
				this.armor[part] = point;
			}
		};

		this.insertMoneyFromCareer = function(object){
			var quantite = ''+object.quantite;
			var dIndex = quantite.indexOf("D");
			if(-1 < dIndex){
				var numberOfDice = quantite.substring(0,dIndex);
				var typeOfDice = quantite.substring(dIndex+1);
				var dice = new Dice();
				var res = 0;

				for(var i=0; i< numberOfDice;i++){
						if(typeOfDice > 10){
							res += dice.D100();
						}
						else{
							res += dice.D10();
						}
				}
				this.purse.co += res;
			}
		};

}
