'use strict';
function Origin(){
	
	this.famille=0;
	this.astral="";
	this.age=0;
	this.naissance="";
	
	this.get=function(infoId){
		var info="";
		
		switch(infoId){
			case "FAMILLE": info=this.famille;break;
			case "ASTRAL": info=this.astral;break;
			case "AGE": info=this.age;break;
			case "NAISSANCE": info=this.naissance;break;

		}
		
		return info;
	};
	
	this.set=function(infoId,info){
	
		
		switch(infoId){
			case "FAMILLE": this.famille=info;break;
			case "ASTRAL": this.astral=info;break;
			case "AGE": this.age=info;break;
			case "NAISSANCE": this.naissance=info;break;

		}
		

	};
	
}