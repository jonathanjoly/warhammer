'use strict';
// Livre de base table 2-6, 2-7, 2-8, 2-9, 2-10
function GeneratePhysicalTrait(){
	
	this.table={};
	
	this.table["SIGNE"] = [ "Marques de vérole", "Marques de vérole", "Marques de vérole", "Marques de vérole", "Marques de vérole", "Rougeaud", "Rougeaud", "Rougeaud", "Rougeaud", "Rougeaud", "Balafre", "Balafre", "Balafre", "Balafre", "Balafre", "Tatouage", "Tatouage", "Tatouage", "Tatouage", "Tatouage", "Boucle d'oreille", "Boucle d'oreille", "Boucle d'oreille", "Boucle d'oreille", "Boucle d'oreille", "Oreille abîmée", "Oreille abîmée", "Oreille abîmée", "Oreille abîmée", "Anneau nasal", "Anneau nasal", "Anneau nasal", "Anneau nasal", "Anneau nasal", "Anneau nasal", "Verrue", "Verrue", "Verrue", "Verrue", "Nez cassé", "Nez cassé", "Nez cassé", "Nez cassé", "Nez cassé", "Nez cassé", "Dent en moins", "Dent en moins", "Dent en moins", "Dent en moins", "Dent en moins", "Dentition crasseuse", "Dentition crasseuse", "Dentition crasseuse", "Dentition crasseuse", "Dentition crasseuse", "Strabisme", "Strabisme", "Strabisme", "Strabisme", "Strabisme", "Sourcil(s) manquant(s)", "Sourcil(s) manquant(s)", "Sourcil(s) manquant(s)", "Sourcil(s) manquant(s)", "Sourcil(s) manquant(s)", "Doigt en moins", "Doigt en moins", "Doigt en moins", "Doigt en moins", "Doigt en moins", "Ongle en moins", "Ongle en moins", "Ongle en moins", "Ongle en moins", "Ongle en moins", "Démarche atypique", "Démarche atypique", "Démarche atypique", "Démarche atypique", "Démarche atypique", "Odeur bizarre", "Odeur bizarre", "Odeur bizarre", "Odeur bizarre", "Gros nez", "Gros nez", "Gros nez", "Gros nez", "Gros nez", "Poireau (grain de beautÃ©)", "Poireau (grain de beautÃ©)", "Poireau (grain de beautÃ©)", "Poireau (grain de beautÃ©)", "Poireau (grain de beautÃ©)", "Calvitie très localisée", "Calvitie très localisée", "Calvitie très localisée", "Calvitie très localisée", "Couleur étrange d'un oeil (ou des deux)", "Couleur étrange d'un oeil (ou des deux)"];
	
	this.table["RACE-ELFE"]={};
	this.table["RACE-ELFE"]["CHEVEUX"] = ["Argenté","Blond","Paille","Blond","Auburn","Châtain clair","Châtain clair","Châtain","Brun","Noir"];
	this.table["RACE-ELFE"]["POIDS"] = [ 36, 38, 38, 40, 40, 43, 43, 43, 45, 45, 45, 45, 47, 47, 47, 47, 47, 49, 49, 49, 49, 49, 52, 52, 52, 52, 52, 52, 52, 54, 54, 54, 54, 54, 54, 54, 54, 56, 56, 56, 56, 56, 56, 56, 56, 56, 56, 56, 56, 59, 59, 59, 59, 59, 59, 59, 59, 59, 59, 59, 59, 59, 59, 59, 61, 61, 61, 61, 61, 61, 61, 63, 63, 63, 63, 63, 63, 63, 65, 65, 65, 65, 65, 68, 68, 68, 68, 68, 70, 70, 70, 70, 72, 72, 72, 74, 74, 77, 77, 79];
	this.table["RACE-ELFE"]["YEUX"] = ["Gris-bleu","Bleu","Vert","Cuivre","Marron clair","Marron","Marron foncé","Argent","Mauve","Noir"]; 
	this.table["RACE-ELFE"]["TAILLE"] = {};
	this.table["RACE-ELFE"]["TAILLE"]["M"] = 165;
	this.table["RACE-ELFE"]["TAILLE"]["F"] = 160;
	
	
	this.table["RACE-HALF"]={};
	this.table["RACE-HALF"]["CHEVEUX"] = ["Blond cendré","Paille","Blond","Blond","Auburn","Roux","Châtain clair","Châtain","Brun","Noir"];
	this.table["RACE-HALF"]["POIDS"] = [ 34, 34, 34, 36, 36, 36, 36, 36, 38, 38, 38, 38, 38, 38, 38, 38, 38, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 43, 43, 43, 43, 43, 43, 43, 43, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 47, 47, 47, 47, 47, 47, 47, 49, 49, 49, 49, 49, 49, 49, 52, 52, 52, 52, 52, 54, 54, 54, 54, 54, 56, 56, 56, 56, 59, 59, 59, 61, 61, 63, 63, 65];
	this.table["RACE-HALF"]["TAILLE"] = {};
	this.table["RACE-HALF"]["TAILLE"]["M"] = 100;
	this.table["RACE-HALF"]["TAILLE"]["F"] = 95;
	this.table["RACE-HALF"]["YEUX"] = ["Bleu","Noisette","Noisette","Marron clair","Marron clair","Marron","Marron","Marron foncé","Marron foncé","Marron foncé"]; 

	this.table["RACE-HUMA"]={};
	this.table["RACE-HUMA"]["CHEVEUX"] = ["Blond cendré","Paille","Blond","Auburn","Roux","Châtain clair","Châtain","Châtain","Brun","Noir"];
	this.table["RACE-HUMA"]["POIDS"] = [ 47, 49, 49, 52, 52, 54, 54, 54, 56, 56, 56, 56, 59, 59, 59, 59, 59, 61, 61, 61, 61, 61, 63, 63, 63, 63, 63, 63, 63, 65, 65, 65, 65, 65, 65, 65, 65, 68, 68, 68, 68, 68, 68, 68, 68, 68, 68, 68, 68, 70, 70, 70, 70, 70, 70, 70, 70, 70, 70, 70, 70, 70, 70, 70, 72, 72, 72, 72, 72, 72, 72, 74, 74, 74, 74, 74, 74, 74, 77, 77, 77, 77, 77, 79, 79, 79, 79, 79, 81, 81, 81, 81, 86, 86, 86, 90, 90, 95, 95, 99];
	this.table["RACE-HUMA"]["TAILLE"] = {};
	this.table["RACE-HUMA"]["TAILLE"]["M"] = 160;
	this.table["RACE-HUMA"]["TAILLE"]["F"] = 152;
	this.table["RACE-HUMA"]["YEUX"] = ["Gris clair","Gris-bleu","Bleu","Vert","Cuivre","Marron clair","Marron","Marron foncé","Mauve","Noir"]; 

	this.table["RACE-NAIN"]={};
	this.table["RACE-NAIN"]["CHEVEUX"] = ["Blond cendré","Blond","Roux","Auburn","Châtain clair","Châtain","Châtain","Brun","Bleu foncé","Noir"];
	this.table["RACE-NAIN"]["POIDS"] = [40, 43, 43, 45, 45, 47, 47, 47, 49, 49, 49, 49, 52, 52, 52, 52, 52, 54, 54, 54, 54, 54, 56, 56, 56, 56, 56, 56, 56, 59, 59, 59, 59, 59, 59, 59, 59, 61, 61, 61, 61, 61, 61, 61, 61, 61, 61, 61, 61, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 65, 65, 65, 65, 65, 65, 65, 68, 68, 68, 68, 68, 68, 68, 70, 70, 70, 70, 70, 72, 72, 72, 72, 72, 74, 74, 74, 74, 77, 77, 77, 79, 79, 81, 81, 83];
	this.table["RACE-NAIN"]["TAILLE"] = {};
	this.table["RACE-NAIN"]["TAILLE"]["M"] = 130;
	this.table["RACE-NAIN"]["TAILLE"]["F"] = 125;
	this.table["RACE-NAIN"]["YEUX"] = ["Gris clair","Bleu","Cuivre","Marron clair","Marron clair","Marron","Marron","Marron foncé","Marron foncé","Mauve"]; 

	
	
	this.generate=function(raceId,sexeId){
		var physicalTrait = new PhysicalTrait();
		var dice = new Dice();

		physicalTrait.set("TAILLE",this.computeHeight(raceId,sexeId,dice.D10()));
		
		physicalTrait.set("CHEVEUX",this.define(raceId,"CHEVEUX",dice.D10()));
		physicalTrait.set("POIDS",this.define(raceId,"POIDS",dice.D100()));
		physicalTrait.set("YEUX",this.define(raceId,"YEUX",dice.D10()));
		physicalTrait.set("SIGNE",this.defineSign(dice.D100())+", "+this.defineSign(dice.D100()));
		return physicalTrait;
	};
	
	this.computeHeight=function(raceId,sexeId,deRes){
		return this.table[raceId]["TAILLE"][sexeId] + (2.5*deRes);
	};
	
	this.define= function(raceId,traitId,deRes){
		return this.table[raceId][traitId][deRes-1];
	};
	
	this.defineSign= function(deRes){

		return this.table["SIGNE"][deRes-1];
	};
}