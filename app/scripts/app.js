'use strict';

/**
 * @ngdoc overview
 * @name warhammerApp
 * @description
 * # warhammerApp
 *
 * Main module of the application.
 */
angular
  .module('warhammerApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/home', {
        templateUrl: 'views/home.html',
        controller: 'HomeCtrl',
        controllerAs: 'home'
      })
      .when('/weapons', {
        templateUrl: 'views/weapons.html',
        controller: 'WeaponsCtrl',
        controllerAs: 'weapons'
      })
      .when('/armors', {
        templateUrl: 'views/armors.html',
        controller: 'ArmorsCtrl',
        controllerAs: 'armors'
      })
      .when('/shop', {
        templateUrl: 'views/shop.html',
        controller: 'ShopCtrl',
        controllerAs: 'shop'
      })
      .when('/carrers', {
        templateUrl: 'views/carrers.html',
        controller: 'CarrersCtrl',
        controllerAs: 'carrers'
      })
      .when('/talents', {
        templateUrl: 'views/talents.html',
        controller: 'TalentsCtrl',
        controllerAs: 'talents'
      })
      .when('/skills', {
        templateUrl: 'views/skills.html',
        controller: 'SkillsCtrl',
        controllerAs: 'skills'
      })
      .when('/objects', {
        templateUrl: 'views/objects.html',
        controller: 'ObjectsCtrl',
        controllerAs: 'objects'
      })
      .when('/races', {
        templateUrl: 'views/races.html',
        controller: 'RacesCtrl',
        controllerAs: 'races'
      })
      .when('/magics', {
        templateUrl: 'views/magics.html',
        controller: 'MagicsCtrl',
        controllerAs: 'magics'
      })
      .when('/create', {
        templateUrl: 'views/create.html',
        controller: 'CreateCtrl',
        controllerAs: 'create'
      })
      .when('/edit', {
        templateUrl: 'views/edit.html',
        controller: 'EditCtrl',
        controllerAs: 'edit'
      })
      .when('/timeForRunes', {
        templateUrl: 'views/timeforrunes.html',
        controller: 'TimeforrunesCtrl',
        controllerAs: 'timeForRunes'
      })
      .otherwise({
        redirectTo: '/home'
      });
  });
